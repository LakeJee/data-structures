@ECHO off

clang -g -v -Wall -Wextra main.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" -Wno-return-stack-address -o HashTableTestDebug.exe

clang -O3  main.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" -Wno-return-stack-address -o HashTableTestRelease.exe


ECHO.


IF EXIST HashTableTestRelease.exe (
	CALL HashTableTestRelease.exe
) ELSE (
	ECHO No executable found.
	ECHO.
)