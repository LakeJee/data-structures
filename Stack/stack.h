/* date = May 9th 2023 11:04 am */

#ifndef STACK_H
#define STACK_H

#include <inttypes.h>

#ifndef U64_TYPE
#define U64_TYPE
typedef uint_fast64_t U64;
#endif

#define Kilobytes(n)  (n << 10)

typedef int8_t   S8;
typedef S8       B8;

typedef enum AllocatorType { standard, custom } AllocatorType;

#ifndef ALLOCATOR
#define ALLOCATOR
typedef struct Allocator Allocator;
struct Allocator {};
#endif

typedef struct StackNode StackNode;
struct StackNode
{
  StackNode *next;
  void      *data;
};

typedef struct Stack Stack;
struct Stack
{
  StackNode *top;
  U64        elementSize;
  U64        elementCount;
  Allocator **allocator; // a pointer to a pointer to the allocator you want to use so you don't accidentally free the the allocator itself
};


/*
* Prototypes
*
*/
// Dynamic Initialization
void  stack_init_dyanmic (Stack **s, U64 elementSize, Allocator *a);
// Static Initialization
Stack stack_init_static (U64 elementSize);
// Utilities
void  set_values (Stack *s, U64 elementSize);
StackNode *create_stack_node (Stack *s, void *data);
B8 is_empty(Stack *s);
// Operations
void  push (Stack *s, void *elem);
void *pop (Stack *s);
// Tear Down
void  destroy (Stack *s);


/*
* Implementation
*
*/
void 
stack_init_dynamic(Stack **s, U64 elementSize, Allocator *a) 
{
  /* Initialize an empty stack */
  if (*s)
  {
    // free the values before we reset them?
    set_values (*s, elementSize);
  } else
  {
    printf("Stack Uninitialized. Initializing now.\n");
    if (a)
    {
      /*
      * Write your own allocation here!
      *
      */
      (*s)->allocator = &a;
      return;
    } else
    {
      printf("No allocator found. Defaulting to malloc.\n");
      *s = malloc (sizeof(Stack));
      if (*s)
      {
        printf("Setting values.\n");
        set_values (*s, elementSize);
      } else
      {
        printf("Error: Failed to initialize Stack. Exiting (1).\n");
        exit(1);
      }
    }
  }
}


Stack
stack_init_static(U64 elementSize)
{
  Stack result = { 0 };

  result.elementSize = elementSize;

  return (result);
}


void
set_values(Stack *s, U64 elementSize)
{
  s->top  = NULL;
  s->elementSize = elementSize;
  s->elementCount = 0;
}


StackNode *
create_stack_node(Stack *s, void *data)
{
  StackNode *result = NULL;

  if (s->allocator)
  {
    // return your allocation here
    // return ();
  } else
  {
    result = malloc (sizeof(StackNode));
    result->next = NULL;
    result->data = data;
  }

  return (result);
}

B8 
is_empty(Stack *s)
{
  return (s->top == NULL);
}

void 
push(Stack *s, void *elem) 
{
  if (!s)
  {
    printf("Error: Stack is null.\n");
    return;
  } else
  {
    if (s->allocator)
    {
      /*
      * Write your own allocation here!
      *
      */
      // s->allocator( << do some allocations >> );
      return;
    } else
    {
      StackNode *insert = create_stack_node(s, elem);
      insert->next = s->top;
      s->top = insert;
    }

    s->elementCount += 1;
  }
}


void * 
pop(Stack *s) 
{
  if(is_empty(s))
  {
    printf("Stack Underflow. Exiting (1).\n");
    exit(1);
  }
  if (s->allocator)
  {
    // free the data here
    return NULL;
  } else
  {
    StackNode *temp = s->top;
    s->top = s->top->next;
    void *popped = temp->data;
    free(temp);
    return (popped);
  }
}

void 
destroy(Stack *s) 
{
  if (s->allocator)
  {
    // do your own free!
    // s->allocator->free_func (s)
    // s->allocator = NULL;
  } else
  {
    free (s->allocator);
    free (s);
  }
} 

/*
* API Macros
*
*/
/*
* Init utilites
*
* StackInitD(st, type, allocator) Dynamic allocation
* StackInitS(type) Static allocation
*/
#define StackInit_Dynamic(st, type, allocator) stack_init_dynamic((&st), sizeof(type), (allocator))
#define StackInit_Static(type)                stack_init_static(sizeof(type))


/*
* Push utilites
*
* StackPush(s, data) Push a single data element onto the stack.
*
* StackPushMany(s, count, array) Push an array of elements onto the stack.
* 
* StackPushWithInitMany(s, type, count, init_func) Push a specified number of elements onto
*                                                  the stack with an initialization function.
*
*/
#define StackPush(s, data) push (s, (void *) data);
#define StackPushMany(s, array) \
for (S8 i = 0; i < (sizeof(array[0])/sizeof(array)); i += 1) { push (&s, (void *)(&array[i])); }
#define StackPushWithInitMany(s, type, count, init_func) \
type array[count]; \
for (S8 i = 0; i < count; i += 1) { array[i] = init_func; } \
for (S8 i = 0; i < count; i += 1) { push (&s, (void *)(&array[i])); }

/*
* Pop utilites
*
* StackPop(s, type) Returns a pointer top element from the stack.
* 
* StackPopMany(s, count, result) Modifies the result array with the number of elements 
*                                specified by count.
*
* StackPopAll(s, result) Modifies the result array with every element from the stack.
*                        The result array needs to get initialized with the _maxElements field
 *                        from the stack struct.
* 
* NOTE: We use __typeof__(x) here for many and all versions of pop b/c why not.
*
*/
#define StackPop(s, type) (type *)pop(&s)
#define StackPopMany(s, count, result) \
for (S8 i = 0; i < count; i += 1) { result[i] = *(((__typeof__(result[i]) *)(pop (&s))); }

/*
* Destroy
*
* StackDestroy(s) Invalidate the stack.
*
*/
#define StackDestroy(s) destroy(&s)

#endif //STACK_H
