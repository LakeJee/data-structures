#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <time.h>

#include "stack.h"

typedef struct SomeData SomeData;
struct SomeData
{
  char *nameOfData;
  int   theData;
};


SomeData 
init_some_data()
{
  SomeData s = { 0 };
  s.nameOfData = "Stack time!";
  s.theData = rand();
  return (s);
}


int
  main ()
{
  printf("Hello Stack!\n");
  
  srand(time(NULL));   // Initialization for rand should only be called once.
  
  Stack *stackD = NULL;
  StackInit_Dynamic(stackD, SomeData, NULL);
  Stack stackS = StackInit_Static(SomeData);
  if (stackD && stackS.elementSize != 0)
  {
    printf("Stack allocated!\n");
  } else
  {
    printf("Fuck.\n");
  }

  //StackPushWithInitMany (stackS, SomeData, 20, init_some_data());
  SomeData d1 = init_some_data();
  StackPush(&stackS, &d1);

  SomeData *result[stackS.elementCount];
  result[0] = (SomeData *) pop(&stackS);
  printf("SomeData name %s and data %d\n", result[0]->nameOfData, result[0]->theData);

  return 0; 
}