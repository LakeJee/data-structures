@ECHO off

clang -g -v -Wall -Wextra main.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" -Wno-return-stack-address -o StackTestDebug.exe

clang -O3  main.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" -Wno-return-stack-address -o StackTestRelease.exe


ECHO.


IF EXIST StackTestRelease.exe (
	CALL StackTestRelease.exe
) ELSE (
	ECHO No executable found.
	ECHO.
)