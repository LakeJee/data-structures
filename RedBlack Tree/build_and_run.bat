@ECHO off

clang -g -v -Wall -Wextra  main.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" -Wno-return-stack-address -o RedBlackTreeTest.exe

clang -O3  main.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" -Wno-return-stack-address -o RedBlackTreeTestRelease.exe


ECHO.


IF EXIST rb_test_pre_delete.txt (
	DEL rb_test_pre_delete.txt
)ELSE (
	ECHO No test output found.
	ECHO.
)


IF EXIST rb_test_post_delete.txt (
	DEL rb_test_post_delete.txt
)ELSE (
	ECHO No test output found.
	ECHO.
)


IF EXIST RedBlackTreeTestRelease.exe (
	CALL RedBlackTreeTest.exe
) ELSE (
	ECHO No executable found.
	ECHO.
)


ECHO.


If EXIST results.txt (
  TYPE results.txt
)ELSE (
  ECHO No results found.
	ECHO.
)