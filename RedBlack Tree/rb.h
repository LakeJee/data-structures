/* date = May 5th 2023 1:28 pm */

#ifndef RB_H
#define RB_H

#include <inttypes.h>

#pragma warning (disable: 4098)

typedef enum COLOR { BLACK, RED } COLOR;

#ifndef U64_TYPE
#define U64_TYPE
typedef uint_fast64_t U64;
#endif

typedef int8_t   S8;
typedef S8       B8;

typedef struct TreeNode TreeNode;
struct TreeNode {
  TreeNode *parent;
  TreeNode *child[2];
  COLOR color;
  U64 blockSize; // 'key'
};

typedef struct Tree Tree;
struct Tree {
  TreeNode *root;
};


/*
* Prototypes
*
*/
U64 child_side(TreeNode *node);
U64 delete_value(Tree *tree, U64 blockSize);
U64 verify_subtree(TreeNode *node, U64 low, U64 high);
TreeNode  create_node(U64 blockSize);
TreeNode *find_node(Tree *tree, U64 blockSize);
TreeNode *minimum(TreeNode *node);
TreeNode *swap_content(TreeNode *a, TreeNode *b);
void set_child(Tree *tree, TreeNode *node, U64 side, TreeNode *child);
void rotate(Tree *tree, TreeNode *node, U64 side);
void insert_fix(Tree *tree, TreeNode *node);
void insert_node(Tree *tree, TreeNode *node);
void free_node(TreeNode *node);
void delete_fix(Tree *tree, TreeNode *parent, U64 side);
void delete_node(Tree *tree, TreeNode *node);
void verify_tree(Tree *tree);


/*
* Implementation
*
*/
U64 child_side(TreeNode *node) { // Return 1 when the node is its parent
  // right-child, 0 otherwise
  return node->parent && node->parent->child[1] == node;
}


U64 delete_value(Tree *tree, U64 blockSize) {
  TreeNode *node = find_node(tree, blockSize);
  if (node) { // found
    delete_node(tree, node);
    return 1;
  }
  return 0;
}


U64 verify_subtree(TreeNode *node, U64 low, U64 high) {
  if (!node) {
    return 0; // OK. Return number of BLACK
  }
  if (node->blockSize < low || node->blockSize > high) {
    printf("node %llu violates BST property. window is (%llu,%llu).\n",
           node->blockSize, low, high);
    exit(1);
  }
  if (node->parent && node->parent->child[child_side(node)] != node) {
    printf("node %llu is not a child of its parent (inconsistency).\n",
           node->blockSize);
    exit(1);
  }
  if ((!node->parent || node->parent->color == RED) && node->color == RED) {
    printf("node %llu violates red property.\n", node->blockSize);
    exit(1);
  }
  U64 black_count1 = verify_subtree(node->child[0], low, node->blockSize);
  U64 black_count2 = verify_subtree(node->child[1], node->blockSize, high);
  if (black_count1 != black_count2) {
    printf("subtree %llu violates black property.\n", node->blockSize);
    exit(1);
  }
  return black_count1 + (node->color == BLACK);
}


TreeNode create_node(U64 blockSize) {
  TreeNode node = { 0 };
  node.parent = node.child[0] = node.child[1]  = NULL;
  node.color = BLACK;
  node.blockSize = blockSize;
  return node;
}


TreeNode *find_node(Tree *tree, U64 blockSize) {
  TreeNode *node = NULL;
  TreeNode *curr = tree->root;
  
  while (curr != NULL) {
    node = curr;
    if (node->blockSize == blockSize) {
      break;
    }
    curr = curr->child[blockSize > curr->blockSize];
  }
  return node;
}


TreeNode *minimum(TreeNode *node) {
  while (node->child[0]) {
    node = node->child[0];
  }
  return node;
}


TreeNode *swap_content(TreeNode *a, TreeNode *b) {
  // Swap key
  U64 blockSize = a->blockSize;
  a->blockSize = b->blockSize;
  b->blockSize = blockSize;
  return b;
}


void set_child(Tree *tree, TreeNode *node, U64 side, TreeNode *child) {
  if (node) {
    node->child[side] = child;
  } else {
    tree->root = child;
  }
  if (child) {
    child->parent = node;
  }
}


void rotate(Tree *tree, TreeNode *node, U64 side) {
  TreeNode *risingChild =
    node->child[1 - side]; // the child that will move one level up
  set_child(tree, node, 1 - side, risingChild->child[side]);
  set_child(tree, node->parent, child_side(node), risingChild);
  set_child(tree, risingChild, side, node);
}


void insert_fix(Tree *tree, TreeNode *node) {
  TreeNode *parent = node->parent;
  node->color = parent ? RED : BLACK;
  if (parent && parent->color == RED) { // red violation
    tree->root->color = BLACK;
    // We have a red-violation as both node and its parent are red.
    TreeNode *grandparent = parent->parent; // Guaranteed to be not NULL
    U64 side = child_side(parent);
    TreeNode *uncle = grandparent->child[1 - side];
    if (uncle && uncle->color == RED) {
      // Parent and Uncle are red
      parent->color = uncle->color = BLACK;
      // DO NOT REMOVE THIS RETURN STATEMENT
      return insert_fix(tree, grandparent); // repeat using recursion 
    }
    // Uncle is black (or NULL)
    if (node == parent->child[1 - side]) { // Node is inner grandchild?
      // Node is inner grandchild: turn it into an outer-grandchild
      // configuration
      rotate(tree, parent, side); // Node is lifted above parent
      node = parent; // swap the naming of the rotated nodes, so node is the
      // lower one
      parent = node->parent;
    }
    // Node is outer grandchild
    rotate(tree, grandparent, 1 - side);
    parent->color = BLACK;
    grandparent->color = RED;
  }
}


void insert_node(Tree *tree, TreeNode *node) {
  TreeNode *leaf = find_node(tree, node->blockSize);
  // Duplicates not allowed (we could, but then better combine in one node)
  if (!leaf || leaf->blockSize != node->blockSize) {
    set_child(tree, leaf, leaf && node->blockSize > leaf->blockSize, node);
    insert_fix(tree, node);
  }
}



void free_node(TreeNode *node) {
  node = NULL; // 'pseudo' free here. simply invalidate the pointer. 
}


void delete_fix(Tree *tree, TreeNode *parent, U64 side) {
  if (!parent) {
    return;
  }
  
  TreeNode *sibling = parent->child[1 - side]; // has black height >= 1
  TreeNode *close_nephew = sibling->child[side];
  
  if (sibling->color == RED) {
    // Case D3: Sibling is red
    rotate(tree, parent, side);
    parent->color = RED;
    sibling->color = BLACK;
    sibling = close_nephew;
    close_nephew = sibling->child[side];
  }
  // Sibling is black
  TreeNode *distant_nephew = sibling->child[1 - side];
  if (distant_nephew && distant_nephew->color == RED) {
    // Case D6: distant nephew is red
    rotate(tree, parent, side);
    sibling->color = parent->color;
    parent->color = distant_nephew->color = BLACK;
    return;
  }
  // Distant nephew is not red
  sibling->color = RED; // common action for cases D2, D4, D5
  if (close_nephew && close_nephew->color == RED) {
    // Case D5: close nephew is red
    rotate(tree, sibling, 1 - side);
    rotate(tree, parent, side);
    close_nephew->color = parent->color;
    parent->color = sibling->color = BLACK;
    return;
  }
  // Nephews are not red
  if (parent->color == BLACK) {
    // Case D2: Parent is black
    // DO NOT REMOVE THIS RETURN STATEMENT
    return delete_fix(tree, parent->parent, child_side(parent));
  }
  // Case D4: parent is red
  parent->color = BLACK;
}


void delete_node(Tree *tree, TreeNode *node) {
  if (node->child[0] && node->child[1]) {
    node = swap_content(node, minimum(node->child[1]));
  }
  TreeNode *child = node->child[!node->child[0]];
  if (node->color == BLACK && child) {
    child->color = BLACK;
  }
  U64 side = child_side(node);
  set_child(tree, node->parent, side, child);
  if (node->color == BLACK && !child) {
    delete_fix(tree, node->parent, side);
  }
  free_node(node);
}


void verify_tree(Tree *tree) { verify_subtree(tree->root, 0, UINT_FAST64_MAX); }


#endif //RB_H
