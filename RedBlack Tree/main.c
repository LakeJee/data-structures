#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <windows.h>
#include <stdbool.h>

#include "rb.h"


/*
* Driver for testing the RedBlack Tree.
*
* The main function populates a TreeNode array with 20000 nodes with a 
* random blockSize(key) value, and randomly deletes 10000 of the 20000 
* nodes from the tree.
* 
* The two generated files are for comparing the number of generated nodes
* to the number of deleted nodes.
*
* I know we spam the use of U64 here, so we could optimize this to make it more
* efficient later down the line.
*
* This implementation was imporved upon/fixed by @trincot:
* (https://stackoverflow.com/questions/76185406/delete-large-number-of-nodes-from-redblack-tree-causes-infinite-loop)
*/
typedef HANDLE Win32Handle;
typedef HFILE  Win32HFile;
typedef DWORD  Win32DWORD;
typedef LPOFSTRUCT Win32OFStruct;
typedef struct Win32FileHandle Win32FileHandle;
struct Win32FileHandle
{
  Win32Handle handle;
  char *fileName;
};

Win32FileHandle
generate_text_file_results(char name[])
{
  Win32FileHandle result = { 0 };
  result.handle = CreateFile(name,                // name of the write
                             GENERIC_WRITE,          // open for writing
                             0,                      // do not share
                             NULL,                   // default security
                             CREATE_ALWAYS,             // create new file only
                             FILE_ATTRIBUTE_NORMAL,  // normal file
                             NULL);                  // no attr. template
  
  if (result.handle)
  {
    printf("Opening/creating %s for writing.\n", name);
    result.fileName = name;
    return (result);
  } else 
  {
    printf("Error opening/creating %s for writing.\n", name);
    return (result);
  }
  
}

B8
write_to_file(Win32FileHandle fileHandle, char contents[])
{
  B8 success = false;
  Win32DWORD dwBytesToWrite = (Win32DWORD)strlen(contents);
  Win32DWORD dwBytesWritten = 0;
  success = WriteFile((Win32Handle) fileHandle.handle,           // open file handle
                      contents,      // start of data to write
                      dwBytesToWrite,  // number of bytes to write
                      &dwBytesWritten, // number of bytes that were written
                      NULL);
  if (!success)
  {
    printf("Error. Unable to write contents to %s.\n", fileHandle.fileName);
  } else 
  {
    if (dwBytesWritten != dwBytesToWrite)
    {
      // This is an error because a synchronous write that results in
      // success (WriteFile returns TRUE) should write all data as
      // requested. This would not necessarily be the case for
      // asynchronous writes.
      printf("Error: dwBytesWritten != dwBytesToWrite.\n");
    }
    else
    {
#if DEBUG
      printf("Success: Wrote %lu bytes to %s successfully.\n", dwBytesWritten, fileHandle.fileName);
#endif
    }
  }
  
  return (success);
}
void 
print_inorder(Tree *t, TreeNode *n, U64 depth) {
  if (n != NULL) {
    print_inorder(t, n->child[1], depth + 1);
    printf("%*s%llu %c \n", 1 + (int)depth * 2, " ", n->blockSize,
           n->color == BLACK ? 'B' : 'r');
    print_inorder(t, n->child[0], depth + 1);
  }
}

void 
print_inorder_no_fancy(Win32FileHandle file, Tree *t, TreeNode *n, U64 depth) {
  if (n != NULL) {
    print_inorder_no_fancy(file, t, n->child[1], depth + 1);
    char buffer[40];
    _snprintf_s (buffer, 40, 40, "%llu \n", n->blockSize);
    write_to_file(file, buffer);
    print_inorder_no_fancy(file, t, n->child[0], depth + 1);
  }
}

void 
print_tree(Tree *t) { print_inorder(t, t->root, 0); }

void
print_tree_no_fancy(Win32FileHandle f, Tree *t) { print_inorder_no_fancy (f, t, t->root, 0); }

void
shuffle(U64 *array, U64  n) {
  for (U64 i = 0; i < n - 1; i++) {
    U64 j = i + rand() / (RAND_MAX / (n - i) + 1);
    U64 t = array[j];
    array[j] = array[i];
    array[i] = t;
  }
}

int main(void) {
  Tree tree = {0};
  
  printf("Hello RB!\n\n");
  
  LARGE_INTEGER frequency;
  LARGE_INTEGER start;
  LARGE_INTEGER end;
  LARGE_INTEGER frequency2;
  LARGE_INTEGER start2;
  LARGE_INTEGER end2;
  double interval;
  srand(time(NULL));
  time_t t = time(NULL);
  char timeBuffer[26];
  char dateBuffer[36];
  char insertionTimeBuffer[100];
  char deletionTimeBuffer[100];
  U64 n = 20000;
  U64 array[20000];
  TreeNode nodes[20000];
  
  ctime_s(timeBuffer, 26, &t);
  _snprintf_s (dateBuffer, 36, 36, "* Date: %s", timeBuffer);
  Win32FileHandle resultsFile = generate_text_file_results("results.txt");
  write_to_file (resultsFile, "***********************************\n");
  write_to_file (resultsFile, "* RedBlackTree Performance Results \n");
  write_to_file (resultsFile, "***********************************\n");
  write_to_file (resultsFile, (char *)dateBuffer);
  write_to_file (resultsFile, "***********************************\n");
  
  for (U64 i = 0; i < n; i++) {
    array[i] = i;
  }
  shuffle(array, n);
  for (U64 i = 0; i < n; i++) {
    nodes[i] = create_node(array[i]);
  }
  QueryPerformanceFrequency(&frequency);
  QueryPerformanceCounter(&start);
  for (U64 i = 0; i < n; i++) {
    insert_node(&tree, &nodes[i]);
    verify_tree(&tree);
  }
  QueryPerformanceCounter(&end);
  interval = (double) (end.QuadPart - start.QuadPart) / frequency.QuadPart;
  _snprintf_s (insertionTimeBuffer, 100, 100, "Insertion Time(20000 elements): %f seconds\n", interval);
  write_to_file (resultsFile, insertionTimeBuffer);
  Win32FileHandle preDelFile = generate_text_file_results("rb_test_pre_delete.txt");
  write_to_file (preDelFile, "************************\n");
  write_to_file (preDelFile, "*      Insertion       *\n");
  write_to_file (preDelFile, "************************\n");
  write_to_file (preDelFile, insertionTimeBuffer);
  write_to_file (preDelFile, "************************\n");
  print_tree_no_fancy(preDelFile, &tree);
  
  
  // Verify tree and shuffle array
  verify_tree(&tree);
  shuffle(array, n);
  
  
  QueryPerformanceFrequency(&frequency2);
  QueryPerformanceCounter(&start2);
  for (U64 i = 0; i < (n / 2); i++) {
    if (!delete_value(&tree, array[i])) {
      printf("Did not find value %llu in the tree", array[i]);
      exit(1);
    }
    verify_tree(&tree);
  }
  QueryPerformanceCounter(&end2);
  interval = (double) (end2.QuadPart - start2.QuadPart) / frequency2.QuadPart;
  _snprintf_s (deletionTimeBuffer, 100, 100, "Deletion Time(20000 elements): %f seconds\n", interval);
  write_to_file (resultsFile, deletionTimeBuffer);
  Win32FileHandle postDelFile = generate_text_file_results("rb_test_post_delete.txt");
  write_to_file (postDelFile, "************************\n");
  write_to_file (postDelFile, "*        Deletion      *\n");
  write_to_file (postDelFile, "************************\n");
  write_to_file (postDelFile, deletionTimeBuffer);
  write_to_file (postDelFile, "************************\n");
  print_tree_no_fancy(postDelFile, &tree);
  
  CloseHandle(resultsFile.handle);
  CloseHandle(preDelFile.handle);
  CloseHandle(postDelFile.handle);
}