/* date = May 11th 2023 1:17 pm */

#ifndef LL_H
#define LL_H

#include <inttypes.h>

#ifndef U64_TYPE
#define U64_TYPE
typedef uint_fast64_t U64;
#endif

typedef int8_t   S8;
typedef S8       B8;

#ifndef ALLOCATOR
#define ALLOCATOR
typedef struct Allocator Allocator;
struct Allocator {};
#endif

#define MAX_LIST_CAPACITY 25000 // default program stack size is 1MB on Windows so we're going to get it as close as possible 

/*
* Linked List
* 
*/

typedef struct LLNode LLNode;
struct LLNode
{
  void   *data; // where we store the data for the incoming data type. hard limit?
  LLNode *next; // next node
};

typedef struct LinkedList LinkedList;
struct LinkedList
{
  U64         size; // length of list
  LLNode     *head; // start of list
  Allocator **allocator;
};

/*
* Prototypes
*
*/ 
LinkedList init_list_static();
void       init_list_dynamic(LinkedList **list, Allocator *allocator);
LLNode    *create_node(LinkedList *list, void *data);
void       append(LinkedList *list, void *data);
void       prepend(LinkedList *list, void *data);
void       delete_node(LinkedList *list, void *data);
void       set_values(LinkedList *list);
void      *search(LinkedList *list, void *data);
LLNode    *pop_head(LinkedList *list);


/*
* Implementation
*
*/ 
void
init_list_dynamic(LinkedList **list, Allocator *allocator)
{
  if (*list)
  {
    // free the values before we reset them?
    set_values(*list);
  } else
  {
    printf("List Uninitialized. Initializing now.\n");
    if (allocator)
    {
      /*
      * Write your own allocation here!
      *
      */
      (*list)->allocator = &allocator;
      return;
    } else
    {
      printf("No allocator found. Defaulting to malloc.\n");
      *list = malloc (sizeof(LinkedList));
      if (*list)
      {
        printf("Setting values.\n");
        set_values (*list);
      } else
      {
        printf("Error: Failed to initialize List. Exiting (1).\n");
        exit(1);
      }
    }
  }
}

LinkedList
init_list_static()
{
  LinkedList result = { 0 };

  return (result);
}

void *
search(LinkedList *list, void *data)
{
  LLNode *current = list->head;
  while (current->data != data)
    current = current->next;

  return (current->data);
}

void
set_values(LinkedList *list)
{
  list->size = 0; // length of list
  list->head = NULL; 
  list->allocator = NULL;
}

LLNode *
create_node(LinkedList *list, void *data)
{
  LLNode *result = NULL;

  if (list->allocator)
  {
    // do your allocation here!
    // allocator
  } else
  {
    result = malloc (sizeof(LLNode));
    result->data = data;
    result->next = NULL;
  }

  return (result);
}

void 
append(LinkedList *list, void *data)
{
  LLNode *node = NULL;
  if (data)
  {
    node = create_node(list, data);
  }

  if (!node)
  {
    printf("Error! Cannot append NULL node!\n");
    return;
  }
  else
  {
    if (!list)
    {
      printf("Error! Cannot append to a NULL list!\n");
    } else
    {
      if (list->head == NULL)
      {
        list->head = node;
        node->next = NULL;
      } else
      {
        LLNode *current = list->head;
        while (current->next != NULL)
          current = current->next;

        current->next = node;
      }

      list->size += 1;
    }
  }
}


void
prepend(LinkedList *list, void *data)
{
  LLNode *node = NULL;
  if (data)
  {
    node = create_node(list, data);
  }

  if (!node)
  {
    printf("Error! Cannot prepend NULL node!\n");
    return;
  } else
  {
    if (!list)
    {
      printf("List is unitialized.\n");
      return;
    }
    if (list->head)
    {
      node->next = list->head;
      list->head = node;
    } else
    {
      list->head = node;
    }
    list->size += 1;
  }
}


void
delete_node (LinkedList *list, void *data)
{
  LLNode *temp = list->head; 
  LLNode *prev = NULL;

  if (temp != NULL && temp->data == data) 
  {
    list->head = temp->next;
    free(temp);
    return;
  }

  while (temp != NULL && temp->data != data) 
  {
    prev = temp;
    temp = temp->next;
  }

  if (temp == NULL)
  {
    printf("Node not found!\n");
    return;
  }

  prev->next = temp->next;

  free(temp);
}


LLNode *
pop_head(LinkedList *list)
{
  LLNode *result = list->head;
  if (list->head->next == NULL || list->size == 1)
  {
    list->head = NULL;
    list->size = 0;
  } else {
    list->head = list->head->next;
    list->size -= 1;
  }
  
  return (result);
}

void
delete_list(LinkedList *list)
{
  LLNode *current = list->head;
  while (current)
  {
    LLNode *toFree = current;
    current = current->next;
    free (toFree);
  }

  if (!current)
  {
    printf("All nodes freed.\n");
  }

  free (list->allocator);
  free (list);
}


/*
* API Macros
*
*/
/*
* Init utilites
*
* InitList_Dynamic(l, a) Dynamic allocation
* InitList_Static() Static allocation
*/
#define InitList_Dynamic(l, a) init_list_dynamic(&(l), (a));
#define InitList_Static() init_list_static();

/*
* Search utilites
*
* SearchList(l, d) Searches for a pointer to some data that lives in the list
*/
#define SearchList(l, d) search((l), (d));

/*
* Push utilites
*
* ListAppend(l, d) Add some data to the end of the list
* ListPrepend(l, d) Add some data to the beginning of the list
*/
#define ListAppend(l, d) append((l), (void *)(d));
#define ListPrepend(l, d) prepend((l), (void *)(d));

/*
* Remove utilites
*
* ListRemove(l, d) Remove some data from the list
* ListPop(l) Remove some data from the front of the list
*/
#define ListRemove(l, d) delete_node((l), (d));
#define ListPop(l) pop_head((l));

/*
* Delete Utility
*
* ListDelete(l) Delete the list
*/
#define ListDelete(l) delete_list((l));

#endif //LL_H
