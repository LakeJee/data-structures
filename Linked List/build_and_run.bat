@ECHO off

clang -g -v -Wall -Wextra  main.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" "-LRpcRT4.Lib" -Wno-return-stack-address -o LinkedListTestDebug.exe

clang -O3 main.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" "-LRpcRT4.Lib" -Wno-return-stack-address -o LinkedListTestRelease.exe


ECHO.


IF EXIST LinkedListTestRelease.exe (
	CALL LinkedListTestRelease.exe
) ELSE (
	ECHO No executable found.
	ECHO.
)