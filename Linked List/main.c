#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <Windows.h>
#include <stdbool.h>
#include <rpcdce.h>

#include "ll.h"

typedef HANDLE Win32Handle;
typedef HFILE  Win32HFile;
typedef DWORD  Win32DWORD;
typedef LPOFSTRUCT Win32OFStruct;
typedef struct Win32FileHandle Win32FileHandle;
struct Win32FileHandle
{
  Win32Handle handle;
  char *fileName;
};

Win32FileHandle
generate_text_file_results(char name[])
{
  Win32FileHandle result = { 0 };
  result.handle = CreateFile(name,                   // name of the write
                             GENERIC_WRITE,          // open for writing
                             0,                      // do not share
                             NULL,                   // default security
                             CREATE_ALWAYS,          // create new file only
                             FILE_ATTRIBUTE_NORMAL,  // normal file
                             NULL);                  // no attr. template
  if (result.handle)
  {
    printf("Opening/creating %s for writing.\n", name);
    result.fileName = name;
    return (result);
  } else
  {
    printf("Error opening/creating %s for writing.\n", name);
    return (result);
  }
  
}

B8
write_to_file(Win32FileHandle fileHandle, char contents[])
{
  B8 success = false;
  Win32DWORD dwBytesToWrite = (Win32DWORD)strlen(contents);
  Win32DWORD dwBytesWritten = 0;
  success = WriteFile((Win32Handle) fileHandle.handle,           // open file handle
                      contents,      // start of data to write
                      dwBytesToWrite,  // number of bytes to write
                      &dwBytesWritten, // number of bytes that were written
                      NULL);
  if (!success)
  {
    printf("Error. Unable to write contents to %s.\n", fileHandle.fileName);
  } else
  {
    if (dwBytesWritten != dwBytesToWrite)
    {
      // This is an error because a synchronous write that results in
      // success (WriteFile returns TRUE) should write all data as
      // requested. This would not necessarily be the case for
      // asynchronous writes.
      printf("Error: dwBytesWritten != dwBytesToWrite.\n");
    }
    else
    {
#if DEBUG
      printf("Success: Wrote %lu bytes to %s successfully.\n", dwBytesWritten, fileHandle.fileName);
#endif
    }
  }
  
  return (success);
}

typedef struct SomeData SomeData;
struct SomeData
{
  char *nameOfData;
  U64 theData;
};

SomeData 
init_some_data()
{
  SomeData s = { 0 };
  s.nameOfData = "List time!";
  s.theData = rand();
  return (s);
}

void
print_some_data(Win32FileHandle f, LinkedList *list)
{
  LLNode *current = list->head;
  while (current->next != NULL)
  {
    char buffer[60];
    _snprintf_s (buffer, 60, 60, "the data's data: %llu\n",((SomeData *)current->data)->theData);
    write_to_file (f, buffer);
    if (current->next)
    {
      current = current->next;
    }
  }
}

int
main()
{
  srand(time(NULL));

  LinkedList *list = NULL;
  InitList_Dynamic(list, NULL)
  if (list)
  {
    printf("Hello LL!\n");
    SomeData ass[20000];
    for (int i = 0; i < 20000; i += 1)
    {
      ass[i] = init_some_data();
    }
    
    LARGE_INTEGER frequency = { 0 };
    LARGE_INTEGER start = { 0 };
    LARGE_INTEGER end = { 0 };
    LARGE_INTEGER frequency2 = { 0 };
    LARGE_INTEGER start2 = { 0 };
    LARGE_INTEGER end2 = { 0 };
    double interval = 0;
    time_t t = time(NULL);
    char timeBuffer[26];
    char dateBuffer[36];
    char insertionTimeBuffer[100];
    char deletionTimeBuffer[100];
    ctime_s(timeBuffer, 26, &t);
    _snprintf_s (dateBuffer, 36, 36, "* Date: %s", timeBuffer);
    
    Win32FileHandle resultsFile = generate_text_file_results("results.txt");
    write_to_file (resultsFile, "***********************************\n");
    write_to_file (resultsFile, "* LinkedList Performance Results   \n");
    write_to_file (resultsFile, "***********************************\n");
    write_to_file (resultsFile, (char *)dateBuffer);
    write_to_file (resultsFile, "***********************************\n");
    QueryPerformanceCounter(&start);
    for (int i = 0; i < 20000; i += 1)
    {
      ListAppend(list, (&ass[i])); 
    }
    QueryPerformanceCounter(&end);
    interval = (double) (end.QuadPart - start.QuadPart) / frequency.QuadPart;
    _snprintf_s (insertionTimeBuffer, 100, 100, "Insertion Time(20000 elements): %f seconds\n", interval);
    write_to_file (resultsFile, insertionTimeBuffer);
    Win32FileHandle preDelFile = generate_text_file_results("ll_test_pre_delete.txt");
    write_to_file (preDelFile, "************************\n");
    write_to_file (preDelFile, "*      Insertion       *\n");
    write_to_file (preDelFile, "************************\n");
    write_to_file (preDelFile, insertionTimeBuffer);
    write_to_file (preDelFile, "************************\n");
    print_some_data (preDelFile, list);

    QueryPerformanceFrequency(&frequency2);
    QueryPerformanceCounter(&start2);
    for (U64 i = 0; i < 10000; i++) 
    {
      ListRemove(list, (&ass[i])); // in order deletion. random index is not gaurenteed to be in the list when run 10k times.
    }

    QueryPerformanceCounter(&end2);
    interval = (double) (end2.QuadPart - start2.QuadPart) / frequency2.QuadPart;
    _snprintf_s (deletionTimeBuffer, 100, 100, "Deletion Time(10000 elements): %f seconds\n", interval);
    write_to_file (resultsFile, deletionTimeBuffer);
    Win32FileHandle postDelFile = generate_text_file_results("ll_test_post_delete.txt");
    write_to_file (postDelFile, "************************\n");
    write_to_file (postDelFile, "*        Deletion      *\n");
    write_to_file (postDelFile, "************************\n");
    write_to_file (postDelFile, deletionTimeBuffer);
    write_to_file (postDelFile, "************************\n");
    print_some_data (postDelFile, list);

    CloseHandle(resultsFile.handle);
    CloseHandle(preDelFile.handle);
    CloseHandle(postDelFile.handle);
    return 0;
  }
}