@ECHO off

cl.exe main.c  /Zi /link User32.lib kernel32.lib Gdi32.lib shell32.lib vcruntime.lib msvcrt.lib libcmt.lib /out:BinarySearchTreeTest.exe


ECHO.


IF EXIST bst_test_pre_delete.txt (
	DEL bst_test_pre_delete.txt
)ELSE (
	ECHO No test output found.
	ECHO.
)


IF EXIST bst_test_post_delete.txt (
	DEL bst_test_post_delete.txt
)ELSE (
	ECHO No test output found.
	ECHO.
)


IF EXIST BinarySearchTreeTest.exe (
	CALL BinarySearchTreeTest.exe
) ELSE (
	ECHO No executable found.
	ECHO.
)